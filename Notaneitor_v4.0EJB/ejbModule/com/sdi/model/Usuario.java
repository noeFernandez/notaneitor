package com.sdi.model;

import java.io.Serializable;

public class Usuario implements Serializable {
	@Override
	public String toString() {
		return "Usuario [id=" + id + ", login=" + login + ", name=" + name
				+ ", apellidos=" + apellidos + ", email=" + email
				+ ", password=" + password + ", rol=" + rol + "]";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 3935612962599460219L;
	
	private Long id;
	private String login;
	private String name;
	private String apellidos;
	private String email;
	private String password;
	private String rol;
	private Boolean activada;
	
	
	
	
	public Boolean getActivada() {
		return activada;
	}

	public void setActivada(Boolean activada) {
		this.activada = activada;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public Usuario(){
		this.activada = false;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getId() {
		return this.id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	

}
