package com.sdi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Profesor extends Usuario implements Serializable{

	private static final long serialVersionUID = -7804151758696126465L;
	
	private List<Asignatura> asignaturas;
	
	public Profesor(){
		super();
	}
	
	public void addAsignatura(Asignatura a){
		if(this.asignaturas == null){
			this.asignaturas = new ArrayList<Asignatura>();
		}
		this.asignaturas.add(a);
	}
	
	public List<Asignatura> getAsignaturas(){
		return this.asignaturas;
	}
	
	
}
