package com.sdi.model;

import java.io.Serializable;

public class Matricula implements Serializable {
	
	
	

	private static final long serialVersionUID = 8078957460829464454L;
	private Integer id;
	private Alumno alumno;
	private String asignatura;
	private String nota;
	
	
	
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public Matricula(){
		
	}

	public String getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(String asignatura) {
		this.asignatura = asignatura;
	}

	public String getNota() {
		if(nota.equals("NC")){
			return "No calificado";
		}else if(nota.equals("NE")){
			return "No presentado";
		}
		return nota;
		
	}

	public void setNota(String nota) {
		this.nota = nota;
	}
	
	

}
