package com.sdi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Asignatura implements Serializable {

	private static final long serialVersionUID = 8078957460829464454L;

	@Override
	public String toString() {
		return "Asignatura [nombre=" + nombre + ", codigo=" + codigo
				+ ", creditos=" + creditos + ", curso=" + curso + ", id=" + id
				+ ", profesores=" + profesores + "]";
	}

	private String nombre;
	private String codigo;
	private String creditos;
	private String curso;
	private Integer id;
	private boolean deletable;

	private void setDeletable() {
		this.deletable = this.profesores == null && this.alumnos == null;
	}

	public boolean isDeletable() {
		setDeletable();
		return this.deletable;
	}

	private List<Profesor> profesores;
	private List<Usuario> alumnos;

	public void addAlumno(Usuario u) {
		if (this.alumnos == null) {
			this.alumnos = new ArrayList<Usuario>();
		}
		this.alumnos.add(u);
	}

	public List<Usuario> getAlumnos() {
		return this.alumnos;
	}

	public Asignatura() {

	}

	public void addProfesor(Profesor p) {
		if (this.profesores == null) {
			this.profesores = new ArrayList<Profesor>();
		}
		this.profesores.add(p);
	}

	public List<Profesor> getProfesores() {
		return profesores;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCreditos() {
		return creditos;
	}

	public void setCreditos(String creditos) {
		this.creditos = creditos;
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
