package com.sdi.business.services;

import com.sdi.model.Usuario;

public interface LoginService {
	public Usuario verify(String login, String password);
}
