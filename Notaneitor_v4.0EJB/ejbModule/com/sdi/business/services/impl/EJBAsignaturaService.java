package com.sdi.business.services.impl;

import java.util.List;

import javax.ejb.Stateless;

import com.sdi.business.services.impl.classes.asignaturas.AddCourse;
import com.sdi.business.services.impl.classes.asignaturas.CalificarAsignatura;
import com.sdi.business.services.impl.classes.asignaturas.DarDeBajaAlumno;
import com.sdi.business.services.impl.classes.asignaturas.DeleteCourse;
import com.sdi.business.services.impl.classes.asignaturas.FindAlumnosNoCursanAsignatura;
import com.sdi.business.services.impl.classes.asignaturas.FindAsignaturas;
import com.sdi.business.services.impl.classes.asignaturas.FindMatriculasPorCurso;
import com.sdi.business.services.impl.classes.asignaturas.FindTeachersNotTeachCourse;
import com.sdi.business.services.impl.classes.asignaturas.ObtenerMatriculasAlumno;
import com.sdi.business.services.impl.classes.asignaturas.SetCourseToTeacher;
import com.sdi.business.services.impl.classes.asignaturas.UpdateCourse;
import com.sdi.business.services.interfaces.local.LocalAsignaturasService;
import com.sdi.business.services.interfaces.remote.RemoteAsignaturasService;
import com.sdi.model.Alumno;
import com.sdi.model.Asignatura;
import com.sdi.model.Matricula;
import com.sdi.model.Profesor;
import com.sdi.model.Usuario;

@Stateless
public class EJBAsignaturaService implements LocalAsignaturasService,RemoteAsignaturasService{

	@Override
	public List<Asignatura> getAsignaturas() {
		return new FindAsignaturas().please();
	}

	@Override
	public void addNewCourse(Asignatura asignatura) {
		 new AddCourse().now(asignatura);
		
	}

	@Override
	public void deleteCourse(Asignatura asig) {
		new DeleteCourse().delete(asig);
		
	}

	@Override
	public void updateCourse(Asignatura asig) {
		new UpdateCourse().update(asig);
		
	}

	@Override
	public List<Usuario> obtenerAlumnosQueNoCursanAsignatura(Asignatura a) {
		return new FindAlumnosNoCursanAsignatura().find(a);
	}

	@Override
	public void asignarAsignaturaAProfesor(Profesor profesor,
			Asignatura asignatura) {
		 new SetCourseToTeacher().asign(profesor, asignatura);
		
	}

	@Override
	public List<Profesor> obtenerProfesoresQueNoImpartenAsignatura(
			Asignatura asigantura) {
		return new FindTeachersNotTeachCourse().find(asigantura);
	}

	@Override
	public List<Matricula> getMatriculasAsignatura(Asignatura asignatura) {
		return new FindMatriculasPorCurso().find(asignatura);
	}

	@Override
	public void calificarAlumno(Alumno alumno, Asignatura asignatura, String nota) {
		 new CalificarAsignatura().calificarAsignatura(alumno, asignatura, nota);
		
	}

	@Override
	public List<Matricula> getMatriculasAlumno(Usuario alumno) {
		return new ObtenerMatriculasAlumno().find(alumno);
	}

	@Override
	public void bajaAlumno(Matricula m) {
		new DarDeBajaAlumno().baja(m);
		
	}
	
	
	
	
	
	
	
	

}
