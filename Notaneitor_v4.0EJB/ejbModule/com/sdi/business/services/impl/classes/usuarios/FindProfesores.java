package com.sdi.business.services.impl.classes.usuarios;

import java.util.List;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Profesor;

/**
 * Busca todos los profesores de la base de datos
 * @author No�
 *
 */
public class FindProfesores {
	
	
	public FindProfesores(){
		
	}
	
	public List<Profesor> find(){
		return Factories.persistence.createUsuarioDao().findProfesores();
	}

}
