package com.sdi.business.services.impl.classes.asignaturas;

import java.util.List;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Asignatura;
import com.sdi.model.Profesor;

public class FindTeachersNotTeachCourse {
	
	public List<Profesor> find(Asignatura asignatura){
		return Factories.persistence.createAsignaturaDao().findTeachersNotTeachCourse(asignatura);
	}

}
