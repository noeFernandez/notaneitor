package com.sdi.business.services.impl.classes.usuarios;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Usuario;

public class ActivationChange {
	
	private Usuario u;
	
	public ActivationChange(Usuario u){
		this.u = u;
	}
	
	public void change(){
		Factories.persistence.createUsuarioDao().changeUserActivationState(u);
	}

}
