package com.sdi.business.services.impl.classes.alumnos;

import java.util.List;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Matricula;
import com.sdi.persistence.gateways.AlumnoDao;

public class MatriculaAlumno {
	
	private String id;
	
	public MatriculaAlumno(String user){
		this.id = user;
	}
	
	public List<Matricula> findMyTranscriptions(){
		AlumnoDao ad = Factories.persistence.createAlumnoDao();
		return ad.findMatriculaByID(id);
		
	}

}
