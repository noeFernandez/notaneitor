package com.sdi.business.services.impl.classes.usuarios;

import java.util.List;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Usuario;

public class FindUsuarios {
	
	public List<Usuario> findAll(){
		return Factories.persistence.createUsuarioDao().findAll();
	}

}
