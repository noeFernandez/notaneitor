package com.sdi.business.services.impl.classes.usuarios;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Usuario;
import com.sdi.security.Cypher;

public class NewUser {
	
	public void add(Usuario u){
		u.setPassword(Cypher.digest(u.getPassword()));
		Factories.persistence.createUsuarioDao().createUser(u);
	}

}
