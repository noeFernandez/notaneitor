package com.sdi.business.services.impl.classes.alumnos;

import com.sdi.business.exception.EntityNotFoundException;
import com.sdi.infrastructure.Factories;
import com.sdi.model.Alumno;
import com.sdi.persistence.exception.NotPersistedException;
import com.sdi.persistence.gateways.AlumnoDao;

public class AlumnosUpdate {

	public void update(Alumno alumno) throws EntityNotFoundException {
		AlumnoDao dao = Factories.persistence.createAlumnoDao();
		try {
			dao.update(alumno);
		}
		catch (NotPersistedException ex) {
			throw new EntityNotFoundException("Alumno no eliminado " + alumno, ex);
		}
	}

}
