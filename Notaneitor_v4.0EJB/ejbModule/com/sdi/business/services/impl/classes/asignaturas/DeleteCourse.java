package com.sdi.business.services.impl.classes.asignaturas;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Asignatura;

public class DeleteCourse {
	
	public void delete(Asignatura asig){
		Factories.persistence.createAsignaturaDao().deleteCourse(asig);
	}
	
}
