package com.sdi.business.services.impl.classes.alumnos;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Asignatura;
import com.sdi.model.Usuario;

public class MatricularAlumno {
	
	public void matricula(Usuario u, Asignatura a){
		Factories.persistence.createAlumnoDao().matricular(u,a);
	}

}
