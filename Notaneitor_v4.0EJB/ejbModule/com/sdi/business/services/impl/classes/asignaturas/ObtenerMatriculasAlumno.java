package com.sdi.business.services.impl.classes.asignaturas;

import java.util.List;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Matricula;
import com.sdi.model.Usuario;

public class ObtenerMatriculasAlumno {
	
	public List<Matricula> find(Usuario alumno){
		return Factories.persistence.createAsignaturaDao().getMatriculaPorAlumno(alumno);
	}

}
