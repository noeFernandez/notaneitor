package com.sdi.business.services.impl.classes.asignaturas;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Alumno;
import com.sdi.model.Asignatura;

public class CalificarAsignatura {
	
	public void calificarAsignatura(Alumno alumno, Asignatura asignatura,String nota){
		Factories.persistence.createAsignaturaDao().calificar(alumno, asignatura, nota);
	}

}
