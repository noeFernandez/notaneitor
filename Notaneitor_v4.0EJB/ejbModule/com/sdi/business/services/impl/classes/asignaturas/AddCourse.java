package com.sdi.business.services.impl.classes.asignaturas;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Asignatura;

public class AddCourse {
	
	public void now(Asignatura a){
		Factories.persistence.createAsignaturaDao().addCourse(a);
	}

}
