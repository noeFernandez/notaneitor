package com.sdi.business.services.impl.classes.usuarios;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Usuario;

/**
 * Busca un usuario en la base de datos
 * @author No�
 *
 */
public class FindUsuario {
	
	private String usuario;
	
	public FindUsuario(String usuario){
		this.usuario = usuario;
	}
	
	public Usuario find(){
		return Factories.persistence.createUsuarioDao().findUserByID(usuario);
	}

}
