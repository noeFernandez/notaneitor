package com.sdi.business.services.impl.classes.usuarios;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Usuario;

public class RemoveUsuario {
	
	public void remove(Usuario u){
		Factories.persistence.createUsuarioDao().removeUser(u);
	}

}
