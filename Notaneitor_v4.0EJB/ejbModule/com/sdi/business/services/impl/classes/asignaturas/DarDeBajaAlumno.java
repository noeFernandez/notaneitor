package com.sdi.business.services.impl.classes.asignaturas;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Matricula;

public class DarDeBajaAlumno {
	
	public void baja(Matricula m){
		Factories.persistence.createAsignaturaDao().darDeBaja(m);
	}

}
