package com.sdi.business.services.impl.classes.alumnos;

import com.sdi.business.exception.EntityNotFoundException;
import com.sdi.infrastructure.Factories;
import com.sdi.persistence.exception.NotPersistedException;
import com.sdi.persistence.gateways.AlumnoDao;

public class AlumnosBaja {

	public void delete(Long id) throws EntityNotFoundException {
		AlumnoDao dao = Factories.persistence.createAlumnoDao();
		try {
			dao.delete(id);
		}
		catch (NotPersistedException ex) {
			throw new EntityNotFoundException("Alumno no eliminado " + id, ex);
		}
	}
}
