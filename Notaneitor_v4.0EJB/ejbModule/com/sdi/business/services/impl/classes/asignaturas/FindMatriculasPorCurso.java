package com.sdi.business.services.impl.classes.asignaturas;

import java.util.List;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Asignatura;
import com.sdi.model.Matricula;

public class FindMatriculasPorCurso {
	
	public List<Matricula> find(Asignatura asignatura){
		
		return Factories.persistence.createAsignaturaDao().getMatriculaPorCurso(asignatura);
		
	}

}
