package com.sdi.business.services.impl.classes.asignaturas;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Asignatura;

public class UpdateCourse {
	
	public void update(Asignatura asig){
		Factories.persistence.createAsignaturaDao().updateCourse(asig);
	}

}
