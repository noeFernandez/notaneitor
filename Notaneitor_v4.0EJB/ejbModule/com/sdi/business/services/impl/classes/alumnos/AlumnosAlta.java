package com.sdi.business.services.impl.classes.alumnos;

import com.sdi.business.exception.EntityAlreadyExistsException;
import com.sdi.infrastructure.Factories;
import com.sdi.model.Alumno;
import com.sdi.persistence.exception.AlreadyPersistedException;
import com.sdi.persistence.gateways.AlumnoDao;

public class AlumnosAlta {

	public void save(Alumno alumno) throws EntityAlreadyExistsException {
		AlumnoDao dao = Factories.persistence.createAlumnoDao();
		try {
			dao.save(alumno);
		}
		catch (AlreadyPersistedException ex) {
			throw new EntityAlreadyExistsException("Alumno ya existe " + alumno, ex);
		}
	}

}
