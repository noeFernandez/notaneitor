package com.sdi.business.services.impl.classes.alumnos;

import com.sdi.business.exception.EntityNotFoundException;
import com.sdi.infrastructure.Factories;
import com.sdi.model.Alumno;
import com.sdi.persistence.gateways.AlumnoDao;

public class AlumnosBuscar {

	public Alumno find(Long id) throws EntityNotFoundException {
		AlumnoDao dao = Factories.persistence.createAlumnoDao();
		Alumno a = dao.findById(id);
		if ( a == null) {
			throw new EntityNotFoundException("No se ha encontrado el alumno");
		}
		
		return a;
	}

}
