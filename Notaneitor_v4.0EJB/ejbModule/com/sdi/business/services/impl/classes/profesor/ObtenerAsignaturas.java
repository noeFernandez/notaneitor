package com.sdi.business.services.impl.classes.profesor;

import java.util.List;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Asignatura;
import com.sdi.model.Profesor;

public class ObtenerAsignaturas {
	
	public List<Asignatura> queImparteUnProfesor(Profesor profesor){
		return Factories.persistence.createAsignaturaDao().getCoursesByTeacher(profesor);
	}

}
