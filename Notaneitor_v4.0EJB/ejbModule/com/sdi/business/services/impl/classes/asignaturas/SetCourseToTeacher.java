package com.sdi.business.services.impl.classes.asignaturas;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Asignatura;
import com.sdi.model.Profesor;

public class SetCourseToTeacher {
	
	public void asign(Profesor profesor, Asignatura asignatura){
		Factories.persistence.createAsignaturaDao().asignCourseToTeacher(profesor, asignatura);
	}

}
