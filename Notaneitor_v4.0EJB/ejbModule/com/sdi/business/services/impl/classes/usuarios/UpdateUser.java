package com.sdi.business.services.impl.classes.usuarios;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Usuario;

public class UpdateUser {
	
	public void Update(Usuario u, String pass1){
		if(pass1.length() != 0){
			u.setPassword(pass1);
		}
		Factories.persistence.createUsuarioDao().updateUser(u);
	}

}
