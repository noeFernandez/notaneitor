package com.sdi.business.services.impl.classes.asignaturas;

import java.util.List;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Asignatura;

public class FindAsignaturas {
	
	public FindAsignaturas(){
		
	}
	
	public List<Asignatura> please(){
		return Factories.persistence.createAsignaturaDao().getAsignaturas();
	}

}
