package com.sdi.business.services.impl;

import java.util.List;

import javax.ejb.Stateless;

import com.sdi.business.services.impl.classes.profesor.ObtenerAsignaturas;
import com.sdi.business.services.interfaces.local.LocalProfesoresService;
import com.sdi.business.services.interfaces.remote.RemoteProfesoresService;
import com.sdi.model.Asignatura;
import com.sdi.model.Profesor;

@Stateless
public class EJBProfesoresService implements LocalProfesoresService, RemoteProfesoresService {

	@Override
	public List<Asignatura> obtenerAsignaturasQueImparteProfesor(
			Profesor profesor) {
		return new ObtenerAsignaturas().queImparteUnProfesor(profesor);
	}


}
