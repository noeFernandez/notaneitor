package com.sdi.business.services.impl;

import javax.ejb.Stateless;

import com.sdi.business.services.interfaces.local.LocalLoginService;
import com.sdi.business.services.interfaces.remote.RemoteLoginService;
import com.sdi.model.Usuario;
import com.sdi.persistence.SimplePersistenceFactory;
import com.sdi.security.Cypher;

@Stateless
public class EJBLoginService implements LocalLoginService, RemoteLoginService {

	Usuario usuario = null;

	@Override
	public Usuario verify(String login, String password) {
		if (!validLogin(login, password)) {
			return null;
		}
		return usuario;
	}

	private boolean validLogin(String login, String password) {
		usuario = new SimplePersistenceFactory().createUsuarioDao()
				.findUserByID(login);
		if(usuario == null){
			return false;
		}
		return usuario.getLogin().equals(login)
				&& usuario.getPassword().equals(Cypher.digest(password))
				&& usuario.getActivada();
	}

}