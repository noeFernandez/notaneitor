package com.sdi.business.services.impl;

import java.util.List;

import javax.ejb.Stateless;

import com.sdi.business.exception.EntityAlreadyExistsException;
import com.sdi.business.exception.EntityNotFoundException;
import com.sdi.business.services.impl.classes.alumnos.AlumnosAlta;
import com.sdi.business.services.impl.classes.alumnos.AlumnosBaja;
import com.sdi.business.services.impl.classes.alumnos.AlumnosBuscar;
import com.sdi.business.services.impl.classes.alumnos.AlumnosListado;
import com.sdi.business.services.impl.classes.alumnos.AlumnosUpdate;
import com.sdi.business.services.impl.classes.alumnos.MatriculaAlumno;
import com.sdi.business.services.impl.classes.alumnos.MatricularAlumno;
import com.sdi.business.services.interfaces.local.LocalAlumnosService;
import com.sdi.business.services.interfaces.remote.RemoteAlumnosService;
import com.sdi.model.Alumno;
import com.sdi.model.Asignatura;
import com.sdi.model.Matricula;
import com.sdi.model.Usuario;

/**
 * Clase de implementación (una de las posibles) del interfaz de la fachada de
 * servicios
 * 
 * @author alb
 * 
 */
@Stateless
public class EJBAlumnosService implements LocalAlumnosService, RemoteAlumnosService {

	@Override
	public List<Alumno> getAlumnos() throws Exception{
		return new AlumnosListado().getAlumnos();
	}

	@Override
	public void saveAlumno(Alumno alumno) throws EntityAlreadyExistsException {
		new AlumnosAlta().save(alumno);
	}

	@Override
	public void updateAlumno(Alumno alumno) throws EntityNotFoundException {
		new AlumnosUpdate().update(alumno);
	}

	@Override
	public void deleteAlumno(Long id) throws EntityNotFoundException {
		new AlumnosBaja().delete(id);
	}

	@Override
	public Alumno findById(Long id) throws EntityNotFoundException {
		return new AlumnosBuscar().find(id);
	}

	@Override
	public List<Matricula> obtenerMatricula(String iduser) {
		return new MatriculaAlumno(iduser).findMyTranscriptions();
	}

	@Override
	public void matricular(Usuario u, Asignatura a ) {
		new MatricularAlumno().matricula(u,a);
		
	}
}
