package com.sdi.business.services.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebService;

import com.sdi.business.services.impl.classes.usuarios.ActivationChange;
import com.sdi.business.services.impl.classes.usuarios.FindProfesores;
import com.sdi.business.services.impl.classes.usuarios.FindUsuario;
import com.sdi.business.services.impl.classes.usuarios.FindUsuarios;
import com.sdi.business.services.impl.classes.usuarios.NewUser;
import com.sdi.business.services.impl.classes.usuarios.RemoveUsuario;
import com.sdi.business.services.impl.classes.usuarios.UpdateUser;
import com.sdi.business.services.interfaces.local.LocalUsuariosService;
import com.sdi.business.services.interfaces.remote.RemoteUsuariosService;
import com.sdi.model.Profesor;
import com.sdi.model.Usuario;

@Stateless
@WebService(name="UsuariosService")
public class EJBUsuariosService implements LocalUsuariosService, RemoteUsuariosService{

	@Override
	public Usuario getUserByID(String id) {
		return new FindUsuario(id).find();
	}

	@Override
	public List<Profesor> getProfesores() {
		return new FindProfesores().find();
	}

	@Override
	public List<Usuario> getUsers() {
		return new FindUsuarios().findAll();
	}

	@Override
	public void addNewUser(Usuario user) {
		new NewUser().add(user);
		
	}

	@Override
	public void updateUser(Usuario user, String pass1) {
		new UpdateUser().Update(user, pass1);
		
	}

	@Override
	public void removeUsuario(Usuario u) {
		new RemoveUsuario().remove(u);
		
	}

	@Override
	public void changeActivationState(Usuario u) {
		new ActivationChange(u).change();
		
	}
	
	
	
	
	
	
	
	

}
