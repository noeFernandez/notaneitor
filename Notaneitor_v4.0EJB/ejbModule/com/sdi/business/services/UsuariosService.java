package com.sdi.business.services;

import java.util.List;

import com.sdi.model.Profesor;
import com.sdi.model.Usuario;

public interface UsuariosService {
	
	public Usuario getUserByID(String id);
	public List<Profesor> getProfesores();
	public List<Usuario> getUsers();
	public void addNewUser(Usuario user);
	public void updateUser(Usuario user, String pass1);
	public void removeUsuario(Usuario u);
	public void changeActivationState(Usuario u);

}
