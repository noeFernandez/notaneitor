package com.sdi.business.services;

import java.util.List;

import com.sdi.model.Asignatura;
import com.sdi.model.Profesor;

public interface ProfesoresService {
	
	public List<Asignatura> obtenerAsignaturasQueImparteProfesor(Profesor profesor);
	
}
