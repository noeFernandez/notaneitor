package com.sdi.business.services.interfaces.local;

import javax.ejb.Local;

import com.sdi.business.services.LoginService;

@Local
public interface LocalLoginService extends LoginService{

}
