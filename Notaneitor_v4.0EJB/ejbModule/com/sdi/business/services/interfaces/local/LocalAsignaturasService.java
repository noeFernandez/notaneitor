package com.sdi.business.services.interfaces.local;

import javax.ejb.Local;

import com.sdi.business.services.AsignaturasService;

@Local
public interface LocalAsignaturasService extends AsignaturasService{

}
