package com.sdi.business.services.interfaces.local;

import javax.ejb.Local;

import com.sdi.business.services.ProfesoresService;

@Local
public interface LocalProfesoresService extends ProfesoresService{

}
