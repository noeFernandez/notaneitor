package com.sdi.business.services.interfaces.local;

import javax.ejb.Local;

import com.sdi.business.services.UsuariosService;

@Local
public interface LocalUsuariosService extends UsuariosService{

}
