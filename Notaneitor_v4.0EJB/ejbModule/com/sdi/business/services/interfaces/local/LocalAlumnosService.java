package com.sdi.business.services.interfaces.local;

import javax.ejb.Local;

import com.sdi.business.services.AlumnosService;

@Local
public interface LocalAlumnosService extends AlumnosService{

}
