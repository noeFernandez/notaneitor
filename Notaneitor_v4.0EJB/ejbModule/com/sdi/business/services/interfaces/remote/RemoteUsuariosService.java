package com.sdi.business.services.interfaces.remote;

import javax.ejb.Remote;

import com.sdi.business.services.UsuariosService;

@Remote
public interface RemoteUsuariosService extends UsuariosService{

}
