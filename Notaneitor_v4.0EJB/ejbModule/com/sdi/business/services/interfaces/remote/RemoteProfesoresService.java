package com.sdi.business.services.interfaces.remote;

import javax.ejb.Remote;

import com.sdi.business.services.ProfesoresService;

@Remote
public interface RemoteProfesoresService extends ProfesoresService{

}
