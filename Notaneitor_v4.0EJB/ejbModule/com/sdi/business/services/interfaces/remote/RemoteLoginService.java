package com.sdi.business.services.interfaces.remote;

import javax.ejb.Remote;

import com.sdi.business.services.LoginService;

@Remote
public interface RemoteLoginService extends LoginService{

}
