package com.sdi.business.services.interfaces.remote;

import javax.ejb.Remote;

import com.sdi.business.services.AsignaturasService;

@Remote
public interface RemoteAsignaturasService extends AsignaturasService{

}
