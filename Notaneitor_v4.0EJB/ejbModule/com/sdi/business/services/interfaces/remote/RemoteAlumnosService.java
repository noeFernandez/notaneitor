package com.sdi.business.services.interfaces.remote;

import javax.ejb.Remote;

import com.sdi.business.services.AlumnosService;

@Remote
public interface RemoteAlumnosService extends AlumnosService{

}
