package com.sdi.business.services;

import java.util.List;

import com.sdi.model.Alumno;
import com.sdi.model.Asignatura;
import com.sdi.model.Matricula;
import com.sdi.model.Profesor;
import com.sdi.model.Usuario;

public interface AsignaturasService {

	public List<Asignatura> getAsignaturas();

	public void addNewCourse(Asignatura asigantura);

	public void deleteCourse(Asignatura asig);
	
	public void updateCourse(Asignatura asig);
	
	public List<Usuario> obtenerAlumnosQueNoCursanAsignatura(Asignatura a );

	public void asignarAsignaturaAProfesor(Profesor profesor,
			Asignatura asignatura);

	public List<Profesor> obtenerProfesoresQueNoImpartenAsignatura(
			Asignatura asigantura);

	public List<Matricula> getMatriculasAsignatura(Asignatura asigantura);

	public void calificarAlumno(Alumno alumno, Asignatura asignatura, String nota);

	public List<Matricula> getMatriculasAlumno(Usuario alumno);

	public void bajaAlumno(Matricula m);
}
