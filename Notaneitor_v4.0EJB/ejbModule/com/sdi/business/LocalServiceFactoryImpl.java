package com.sdi.business;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.sdi.business.services.AlumnosService;
import com.sdi.business.services.AsignaturasService;
import com.sdi.business.services.LoginService;
import com.sdi.business.services.ProfesoresService;
import com.sdi.business.services.UsuariosService;

public class LocalServiceFactoryImpl implements ServicesFactory {

	private static final String ALUMNOS_SERVICE_JNDI_KEY = "ejb:"
			+ "Notaneitor_v4.0/" + "Notaneitor_v4.0EJB/"
			+ "EJBAlumnosService!"
			+ "com.sdi.business.services.interfaces.local.LocalAlumnosService";

	
	private static final String USUARIOS_SERVICE_JNDI_KEY = "ejb:"
			+ "Notaneitor_v4.0/" + "Notaneitor_v4.0EJB/"
			+ "EJBUsuariosService!"
			+ "com.sdi.business.services.interfaces.local.LocalUsuariosService";

	private static final String ASIGNATURAS_SERVICE_JNDI_KEY = "ejb:"
			+ "Notaneitor_v4.0/"
			+ "Notaneitor_v4.0EJB/"
			+ "EJBAsignaturaService!"
			+ "com.sdi.business.services.interfaces.local.LocalAsignaturasService";

	private static final String PROFESORES_SERVICE_JNDI_KEY = "ejb:"
			+ "Notaneitor_v4.0/"
			+ "Notaneitor_v4.0EJB/"
			+ "EJBProfesoresService!"
			+ "com.sdi.business.services.interfaces.local.LocalProfesoresService";
	private static final String LOGIN_SERVICE_JNDI_KEY = "ejb:"
			+ "Notaneitor_v4.0/" + "Notaneitor_v4.0EJB/" + "EJBLoginService!"
			+ "com.sdi.business.services.interfaces.local.LocalLoginService";

	@Override
	public AlumnosService createAlumnoService() {
		try {
			Context ctx = new InitialContext();
			return (AlumnosService) ctx.lookup(ALUMNOS_SERVICE_JNDI_KEY);
		} catch (NamingException e) {
			throw new RuntimeException("JNDI problem", e);
		}
	}

	@Override
	public LoginService createLoginService() {
		try {
			Context ctx = new InitialContext();
			return (LoginService) ctx.lookup(LOGIN_SERVICE_JNDI_KEY);
		} catch (NamingException e) {
			throw new RuntimeException("JNDI problem", e);
		}
	}

	@Override
	public UsuariosService createUsuariosService() {
		try {
			Context ctx = new InitialContext();
			return (UsuariosService) ctx.lookup(USUARIOS_SERVICE_JNDI_KEY);
		} catch (NamingException e) {
			throw new RuntimeException("JNDI problem", e);
		}
	}

	@Override
	public AsignaturasService createAsignaturasService() {
		try {
			Context ctx = new InitialContext();
			return (AsignaturasService) ctx
					.lookup(ASIGNATURAS_SERVICE_JNDI_KEY);
		} catch (NamingException e) {
			throw new RuntimeException("JNDI problem", e);
		}
	}

	@Override
	public ProfesoresService createProfesoresService() {
		try {
			Context ctx = new InitialContext();
			return (ProfesoresService) ctx.lookup(PROFESORES_SERVICE_JNDI_KEY);
		} catch (NamingException e) {
			throw new RuntimeException("JNDI problem", e);
		}
	}

}
