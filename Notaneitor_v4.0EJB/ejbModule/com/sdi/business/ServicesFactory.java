package com.sdi.business;

import com.sdi.business.services.AlumnosService;
import com.sdi.business.services.AsignaturasService;
import com.sdi.business.services.LoginService;
import com.sdi.business.services.ProfesoresService;
import com.sdi.business.services.UsuariosService;

/**
 * 
 * @author No�
 *
 */
public interface ServicesFactory {
	
	LoginService createLoginService();
	AlumnosService createAlumnoService();
	UsuariosService createUsuariosService();
	AsignaturasService createAsignaturasService();
	ProfesoresService createProfesoresService();
}
