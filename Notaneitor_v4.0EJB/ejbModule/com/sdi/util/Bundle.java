package com.sdi.util;

import java.util.ResourceBundle;

import javax.faces.context.FacesContext;

public class Bundle {

	public static ResourceBundle getCurrentBundle() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		return facesContext.getApplication().getResourceBundle(facesContext,
				"msgs");
	}

}
