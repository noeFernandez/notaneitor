package com.sdi.persistence.gateways.persistencefachade;

import java.util.List;

import com.sdi.model.Profesor;
import com.sdi.model.Usuario;
import com.sdi.persistence.gateways.UsuarioDao;
import com.sdi.persistence.gateways.impl.UsuarioJDBC;

public class UsuarioJdbcDAO implements UsuarioDao{
	
	private UsuarioJDBC persistenceUnit = null;
	
	

	public UsuarioJDBC getPersistenceUnit() {
		return persistenceUnit;
	}



	public void setPersistenceUnit(UsuarioJDBC persistenceUnit) {
		this.persistenceUnit = persistenceUnit;
	}

	
	public UsuarioJdbcDAO(){
		setPersistenceUnit(new UsuarioJDBC());
	}


	@Override
	public Usuario findUserByID(String id) {
		return persistenceUnit.findByID(id);
	}



	@Override
	public List<Profesor> findProfesores() {
		return persistenceUnit.findProfesores();
	}



	@Override
	public List<Usuario> findAll() {
		return persistenceUnit.findAll();
	}



	@Override
	public void createUser(Usuario u) {
		persistenceUnit.addUser(u);
		
	}



	@Override
	public void updateUser(Usuario u) {
		persistenceUnit.editUser(u);
		
	}



	@Override
	public void removeUser(Usuario u) {
		persistenceUnit.removeUsuario(u);
		
	}



	@Override
	public void changeUserActivationState(Usuario u) {
		persistenceUnit.changeActivationState(u);
		
	}
	
	
	
	
	
	
	
	
	
	

}
