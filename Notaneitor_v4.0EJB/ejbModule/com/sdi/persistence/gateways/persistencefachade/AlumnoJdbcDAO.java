package com.sdi.persistence.gateways.persistencefachade;

import java.util.List;

import com.sdi.model.Alumno;
import com.sdi.model.Asignatura;
import com.sdi.model.Matricula;
import com.sdi.model.Usuario;
import com.sdi.persistence.exception.AlreadyPersistedException;
import com.sdi.persistence.exception.NotPersistedException;
import com.sdi.persistence.gateways.AlumnoDao;
import com.sdi.persistence.gateways.impl.AlumnoJdbc;

public class AlumnoJdbcDAO implements AlumnoDao {
	
	private AlumnoJdbc jdbcAlumno = null; 
	
	public AlumnoJdbc getJdbcAlumno() {
		return jdbcAlumno;
	}
	public void setJdbcAlumno(AlumnoJdbc jdbcAlumno) {
		this.jdbcAlumno = jdbcAlumno;
	}
	
	public AlumnoJdbcDAO() {
		setJdbcAlumno(new AlumnoJdbc());
	}
	
	public List<Alumno> getAlumnos() {
		return new AlumnoJdbc().getAlumnos();
	}

	@Override
	public void save(Alumno a) throws AlreadyPersistedException {
		jdbcAlumno.save(a);		
	}

	@Override
	public void update(Alumno a) throws NotPersistedException {
		jdbcAlumno.update(a);
		
	}

	@Override
	public void delete(Long id) throws NotPersistedException {
		jdbcAlumno.delete(id);
		
	}

	@Override
	public Alumno findById(Long id) {
		return jdbcAlumno.findById(id);
	}
	@Override
	public List<Matricula> findMatriculaByID(String id) {
		return jdbcAlumno.findMatriculaByID(id);
	}
	@Override
	public void matricular(Usuario u, Asignatura a) {
		jdbcAlumno.matricular(u,a);
		
	}
	
}
