package com.sdi.persistence.gateways.persistencefachade;

import java.util.List;

import com.sdi.model.Alumno;
import com.sdi.model.Asignatura;
import com.sdi.model.Matricula;
import com.sdi.model.Profesor;
import com.sdi.model.Usuario;
import com.sdi.persistence.gateways.AsignaturaDao;
import com.sdi.persistence.gateways.impl.AsignaturaJDBC;

public class AsignaturaJdbcDAO implements AsignaturaDao {

	private AsignaturaJDBC persistenceUnit = null;

	public AsignaturaJdbcDAO() {
		setPersistenceUnit(new AsignaturaJDBC());
	}

	public AsignaturaJDBC getPersistenceUnit() {
		return persistenceUnit;
	}

	public void setPersistenceUnit(AsignaturaJDBC persistenceUnit) {
		this.persistenceUnit = persistenceUnit;
	}

	@Override
	public List<Asignatura> getAsignaturas() {
		return persistenceUnit.findAll();
	}

	@Override
	public void addCourse(Asignatura a) {
		persistenceUnit.addCourse(a);
		
	}

	@Override
	public void deleteCourse(Asignatura asig) {
		persistenceUnit.removeCourse(asig);
		
	}

	@Override
	public void updateCourse(Asignatura asig) {
		persistenceUnit.updateCourse(asig);
		
	}

	@Override
	public List<Usuario> findAlumnosNoCursanAsignatura(Asignatura a) {
		return persistenceUnit.findAlumnosNoCursanAsignatura(a);
		
	}

	@Override
	public List<Asignatura> getCoursesByTeacher(Profesor profesor) {
		return persistenceUnit.findCoursesByTeacher(profesor);
	}

	@Override
	public void asignCourseToTeacher(Profesor profesor, Asignatura asignatura) {
		persistenceUnit.asignCourseToTeacher(profesor, asignatura);
	}

	@Override
	public List<Profesor> findTeachersNotTeachCourse(Asignatura asignatura) {
		return persistenceUnit.findTeachersNotTeachCourse(asignatura);
	}

	@Override
	public List<Matricula> getMatriculaPorCurso(Asignatura asignatura) {
		return persistenceUnit.getMatriculasPorCurso(asignatura);
	}

	@Override
	public void calificar(Alumno alumno, Asignatura asignatura, String nota) {
		 persistenceUnit.calificar(alumno, asignatura, nota);
	}

	@Override
	public List<Matricula> getMatriculaPorAlumno(Usuario alumno) {
		return persistenceUnit.getMatriculasPorAlumno(alumno);
		
	}

	@Override
	public void darDeBaja(Matricula m) {
		persistenceUnit.baja(m);
	}
	
	
	
	
	
	
	
	
	

}
