package com.sdi.persistence.gateways;

import java.util.List;

import com.sdi.model.Alumno;
import com.sdi.model.Asignatura;
import com.sdi.model.Matricula;
import com.sdi.model.Profesor;
import com.sdi.model.Usuario;

public interface AsignaturaDao {

	public List<Asignatura> getAsignaturas();

	public void addCourse(Asignatura a);

	public void deleteCourse(Asignatura asig);

	public void updateCourse(Asignatura asig);

	public List<Usuario> findAlumnosNoCursanAsignatura(Asignatura a);

	public List<Asignatura> getCoursesByTeacher(Profesor profesor);

	public void asignCourseToTeacher(Profesor profesor, Asignatura asignatura);

	public List<Profesor> findTeachersNotTeachCourse(Asignatura asignatura);

	public List<Matricula> getMatriculaPorCurso(Asignatura asignatura);

	public void calificar(Alumno alumno, Asignatura asignatura, String nota);

	public List<Matricula> getMatriculaPorAlumno(Usuario alumno);

	public void darDeBaja(Matricula m);
	
	
}
