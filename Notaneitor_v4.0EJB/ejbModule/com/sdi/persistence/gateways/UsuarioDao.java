package com.sdi.persistence.gateways;

import java.util.List;

import com.sdi.model.Profesor;
import com.sdi.model.Usuario;

public interface UsuarioDao {
	
	public Usuario findUserByID(String id);

	public List<Profesor> findProfesores();

	public List<Usuario> findAll();

	public void createUser(Usuario u);

	public void updateUser(Usuario u);

	public void removeUser(Usuario u);

	public void changeUserActivationState(Usuario u);


}
