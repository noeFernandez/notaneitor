package com.sdi.persistence.gateways.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sdi.model.Asignatura;
import com.sdi.model.Profesor;
import com.sdi.model.Usuario;
import com.sdi.persistence.exception.PersistenceException;
import com.sdi.persistence.gateways.JdbcHelper;

public class UsuarioJDBC {

	private static String CONFIG_FILE = "/persistence.properties";
	private JdbcHelper jdbc = new JdbcHelper(CONFIG_FILE);

	public Usuario findByID(String id) {
		Usuario usuario = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		String consulta = "select u.id as id, u.user as user, u.nombre as nombre, u.apellidos as apellidos, u.email as email, u.password as password, u.rol as rol, u.activada as activada from usuario u where u.user = ?";

		try {
			conn = jdbc.createConnection();
			ps = conn.prepareStatement(consulta);
			ps.setString(1, id);
			rs = ps.executeQuery();

			while (rs.next()) {
				usuario = new Usuario();
				usuario.setName(rs.getString("nombre"));
				usuario.setApellidos(rs.getString("apellidos"));
				usuario.setEmail(rs.getString("email"));
				usuario.setLogin(rs.getString("user"));
				usuario.setPassword(rs.getString("password"));
				usuario.setRol(rs.getString("rol"));
				usuario.setId(rs.getLong("id"));
				usuario.setActivada(rs.getBoolean("activada"));
			}
			System.out.println("Encontre");
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception ex) {
				}
			}
			;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception ex) {
				}
			}
			;
		}
		return usuario;
	}

	/**
	 * Obtiene todos los profesores de la base de datos
	 * @return
	 */
	public List<Profesor> findProfesores() {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		List<Profesor> profesores = null;
		String consulta = "SELECT u.nombre as nombre, u .apellidos as apellidos, u.email as email, u.user as user FROM USUARIO u WHERE u.rol = 'profesor'";
		
		try{
			con = jdbc.createConnection();
			ps = con.prepareStatement(consulta);
			rs = ps.executeQuery();
			
			profesores = new ArrayList<Profesor>();
			
			/**
			 * Obtenemos los profesores
			 */
			while(rs.next()){
				Profesor p = new Profesor();
				p.setName(rs.getString("nombre"));
				p.setApellidos(rs.getString("apellidos"));
				p.setEmail(rs.getString("email"));
				p.setLogin(rs.getString("user"));
				
				
				/**
				 * Obtenemos las asignaturas que imparten
				 */
				PreparedStatement psAsignaturas = con.prepareStatement("SELECT a.id as id,a.codigo as code, a.nombre as nombre, a.creditos as creditos, a.curso as curso FROM asignatura a, imparte i where i.profesor = ? and a.codigo = i.asigantura");
				psAsignaturas.setString(1, p.getLogin());
				ResultSet rsAsignaturas = psAsignaturas.executeQuery();
				
				while(rsAsignaturas.next()){
					Asignatura a = new Asignatura();
					a.setCodigo(rsAsignaturas.getString("code"));
					a.setNombre(rsAsignaturas.getString("nombre"));
					a.setCurso(rsAsignaturas.getString("curso"));
					a.setCreditos(rsAsignaturas.getString("creditos"));
					a.setId(rsAsignaturas.getInt("id"));
					p.addAsignatura(a);
				}
				
				profesores.add(p);
				
			}
			
			
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception ex) {
				}
			}
			;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}
		return profesores;
	}

	public List<Usuario> findAll() {
		PreparedStatement ps =null;
		ResultSet rs=  null;
		Connection con = null;
		List<Usuario> usuarios;
		
		try{
			con = jdbc.createConnection();
			
			ps = con.prepareStatement("SELECT u.id as id, u.user as usuario, nombre,apellidos, email, password, rol, activada FROM usuario u ORDER BY ACTIVADA");
			rs = ps.executeQuery();
			
			usuarios = new ArrayList<Usuario>();
			
			
			while(rs.next()){
				Usuario u = new Usuario();
				u.setLogin(rs.getString("usuario"));
				u.setName(rs.getString("nombre"));
				u.setApellidos(rs.getString("apellidos"));
				u.setEmail(rs.getString("email"));
				u.setId(rs.getLong("id"));
				u.setPassword(rs.getString("password"));
				u.setRol(rs.getString("rol"));
				u.setActivada(rs.getBoolean("activada"));
				usuarios.add(u);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception ex) {
				}
			}
			;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}
	
		return  usuarios;
	}

	public void addUser(Usuario user) {
		PreparedStatement ps =null;
		Connection con = null;
		
		try{
			con = jdbc.createConnection();
			ps = con.prepareStatement("INSERT INTO USUARIO(USER, NOMBRE,APELLIDOS, EMAIL, PASSWORD, ROL,ACTIVADA)VALUES ( ?, ?, ?, ?, ?, ?,?)");
			ps.setString(1, user.getLogin());
			ps.setString(2, user.getName());
			ps.setString(3, user.getApellidos());
			ps.setString(4, user.getEmail());
			ps.setString(5, user.getPassword());
			ps.setString(6, user.getRol());
			ps.setBoolean(7, user.getActivada());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}
		
	}

	public void editUser(Usuario u) {
		PreparedStatement ps = null;
		Connection con = null;
		
		try{
			con = jdbc.createConnection();
			ps = con.prepareStatement("UPDATE USUARIO SET USER = ? , NOMBRE = ? , APELLIDOS = ?, EMAIL = ? , PASSWORD = ? WHERE ID = ?");
			ps.setString(1, u.getLogin());
			ps.setString(2, u.getName());
			ps.setString(3, u.getApellidos());
			ps.setString(4, u.getEmail());
			ps.setString(5, u.getPassword());
			ps.setLong(6, u.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}
		
		
	}

	public void removeUsuario(Usuario u) {
		Connection con = null;
		PreparedStatement ps = null;
		
		try{
			con = jdbc.createConnection();
			ps = con.prepareStatement("DELETE FROM USUARIO WHERE ID = ?");
			ps.setLong(1, u.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}
		
	}

	public void changeActivationState(Usuario u) {
		PreparedStatement ps = null;
		Connection con = null;
		
		try{
			con = jdbc.createConnection();
			ps = con.prepareStatement("UPDATE USUARIO SET ACTIVADA = ? WHERE ID = ?");
			ps.setBoolean(1, !u.getActivada());
			ps.setLong(2, u.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}
		
	}
	

}
