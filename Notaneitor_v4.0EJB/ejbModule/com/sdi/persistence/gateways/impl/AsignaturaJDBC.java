package com.sdi.persistence.gateways.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sdi.model.Alumno;
import com.sdi.model.Asignatura;
import com.sdi.model.Matricula;
import com.sdi.model.Profesor;
import com.sdi.model.Usuario;
import com.sdi.persistence.exception.PersistenceException;
import com.sdi.persistence.gateways.JdbcHelper;

public class AsignaturaJDBC {

	private static String CONFIG_FILE = "/persistence.properties";
	private JdbcHelper jdbc = new JdbcHelper(CONFIG_FILE);

	public List<Asignatura> findAll() {

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		List<Asignatura> asignaturas = null;
		String consulta = "SELECT a.id as id, a.codigo as codigo, a.nombre as nombre, a.creditos as creditos, a.curso as curso  FROM ASIGNATURA a";

		try {
			con = jdbc.createConnection();
			ps = con.prepareCall(consulta);
			rs = ps.executeQuery();

			asignaturas = new ArrayList<Asignatura>();

			while (rs.next()) {
				Asignatura a = new Asignatura();
				a.setCodigo(rs.getString("codigo"));
				a.setNombre(rs.getString("nombre"));
				a.setCreditos(rs.getString("creditos"));
				a.setCurso(rs.getString("curso"));
				a.setId(rs.getInt("id"));
				PreparedStatement psAsignaturas = con
						.prepareStatement("SELECT u.user as user, u.nombre as nombre, u.apellidos as apellidos, u.email as email FROM Usuario u, Imparte i where i.profesor = u.user and i.asigantura = ?");
				psAsignaturas.setString(1, a.getCodigo());
				ResultSet rsAsig = psAsignaturas.executeQuery();

				while (rsAsig.next()) {
					Profesor p = new Profesor();
					p.setLogin(rsAsig.getString("user"));
					p.setName(rsAsig.getString("nombre"));
					p.setApellidos(rsAsig.getString("apellidos"));
					p.setEmail(rsAsig.getString("email"));
					a.addProfesor(p);
				}
				
				
				/* A�adimos los alumnos si los hubiere */
				
				PreparedStatement psAlum = con.prepareStatement("select m.ID as id from matricula m where asignatura = ?");
				psAlum.setString(1, a.getCodigo());
				ResultSet rsAlum = psAlum.executeQuery();
				while(rsAlum.next()){
					Usuario u = new Usuario();
					u.setId(rsAlum.getLong("id"));
					a.addAlumno(u);
				}
				asignaturas.add(a);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception ex) {
				}
			}
			;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}

		return asignaturas;
	}

	public void addCourse(Asignatura a) {
		PreparedStatement ps = null;
		Connection con = null;

		try {
			con = jdbc.createConnection();
			ps = con.prepareStatement("INSERT INTO ASIGNATURA (CODIGO,NOMBRE,CREDITOS,CURSO) VALUES (?, ?, ?, ?)");
			ps.setString(1, a.getCodigo());
			ps.setString(2, a.getNombre());
			ps.setString(3, a.getCreditos());
			ps.setString(4, a.getCurso());
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}

	}

	public void removeCourse(Asignatura asig) {
		PreparedStatement ps = null;
		Connection con = null;

		try {
			con = jdbc.createConnection();
			ps = con.prepareStatement("DELETE FROM ASIGNATURA WHERE ID = ?");
			ps.setInt(1, asig.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}

	}

	public void updateCourse(Asignatura asig) {
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = jdbc.createConnection();
			ps = con.prepareStatement("UPDATE ASIGNATURA SET NOMBRE = ?, CREDITOS = ?, CURSO = ? WHERE CODIGO = ?");
			ps.setString(1, asig.getNombre());
			ps.setString(2, asig.getCreditos());
			ps.setString(3, asig.getCurso());
			ps.setString(4, asig.getCodigo());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}

	}

	public List<Usuario> findAlumnosNoCursanAsignatura(Asignatura a) {
		PreparedStatement ps = null;
		Connection con = null;
		String consulta = "select u.id as id, u.user as user, u.nombre as nombre, u.apellidos as apellidos, u.email as email from usuario u where u.user not in (select i.alumno from matricula i where asignatura = ?) and rol = 'alumno'";
		ResultSet rs = null;
		List<Usuario> usuarios = null;

		try {
			con = jdbc.createConnection();
			ps = con.prepareStatement(consulta);
			ps.setString(1, a.getCodigo());
			rs = ps.executeQuery();

			usuarios = new ArrayList<Usuario>();
			while (rs.next()) {
				Usuario u = new Usuario();
				u.setId(rs.getLong("id"));
				u.setName(rs.getString("nombre"));
				u.setApellidos(rs.getString("apellidos"));
				u.setEmail(rs.getString("email"));
				u.setLogin(rs.getString("user"));
				usuarios.add(u);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}
		return usuarios;

	}

	public List<Asignatura> findCoursesByTeacher(Profesor profesor) {
		return null;
	}

	public void asignCourseToTeacher(Profesor profesor, Asignatura asignatura) {
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = jdbc.createConnection();
			ps = con.prepareStatement("INSERT INTO IMPARTE(PROFESOR, ASIGANTURA) VALUES (?,?)");
			ps.setString(1, profesor.getLogin());
			ps.setString(2, asignatura.getCodigo());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}

	}

	public List<Profesor> findTeachersNotTeachCourse(Asignatura asignatura) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		List<Profesor> profesores = null;
		String consulta = "SELECT u.id as id, u.user as user, u.nombre as nombre, u.apellidos as apellidos, u.email as email, u.user as user from usuario u where u.user not in (SELECT i.PROFESOR from IMPARTE i where i.asigantura = ?) and u.rol = 'profesor'";

		try {
			con = jdbc.createConnection();
			ps = con.prepareStatement(consulta);
			ps.setString(1, asignatura.getCodigo());
			rs = ps.executeQuery();

			profesores = new ArrayList<Profesor>();
			while (rs.next()) {
				Profesor p = new Profesor();
				p.setId(rs.getLong("id"));
				p.setName(rs.getString("nombre"));
				p.setApellidos(rs.getString("apellidos"));
				p.setEmail(rs.getString("email"));
				p.setLogin(rs.getString("user"));
				profesores.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}

		return profesores;
	}

	public List<Matricula> getMatriculasPorCurso(Asignatura asignatura) {
		PreparedStatement ps = null;
		Connection con = null;
		ResultSet rs = null;
		List<Matricula> matriculas = null;

		try {
			con = jdbc.createConnection();
			ps = con.prepareStatement("SELECT u.id as id, u.user as user, u.nombre as nombre, u.apellidos as apellidos, u.email as email, m.asignatura as asignatura, m.nota as nota FROM matricula m inner join usuario u on u.user = m.alumno WHERE m.asignatura = ?");
			ps.setString(1, asignatura.getCodigo());
			rs = ps.executeQuery();

			matriculas = new ArrayList<Matricula>();

			while (rs.next()) {
				Alumno alumno = new Alumno();
				Matricula matricula = new Matricula();
				alumno.setId(rs.getLong("id"));
				alumno.setNombre(rs.getString("nombre"));
				alumno.setApellidos(rs.getString("apellidos"));
				alumno.setIduser(rs.getString("user"));
				alumno.setEmail(rs.getString("email"));
				matricula.setAlumno(alumno);
				matricula.setAsignatura(rs.getString("asignatura"));
				matricula.setNota(rs.getString("nota"));
				matriculas.add(matricula);

			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}
		return matriculas;
	}

	public void calificar(Alumno alumno, Asignatura asignatura, String nota) {
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = jdbc.createConnection();
			ps = con.prepareStatement("UPDATE MATRICULA SET NOTA = ? WHERE ASIGNATURA = ? AND ALUMNO = ?");
			ps.setString(1, nota);
			ps.setString(2, asignatura.getCodigo());
			ps.setString(3, alumno.getIduser());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}
	}

	public List<Matricula> getMatriculasPorAlumno(Usuario alumno) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		List<Matricula> matricula = null;

		try {
			con = jdbc.createConnection();
			ps = con.prepareStatement("select id, asignatura , nota from matricula where alumno = ?");
			ps.setString(1, alumno.getLogin());
			rs = ps.executeQuery();

			matricula = new ArrayList<Matricula>();
			while (rs.next()) {
				Matricula m = new Matricula();
				m.setId(rs.getInt("id"));
				m.setAsignatura(rs.getString("asignatura"));
				m.setNota(rs.getString("nota"));
				matricula.add(m);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}

		return matricula;

		
	}

	public void baja(Matricula m) {
		PreparedStatement ps = null;
		Connection con = null;
		
		try{
			con = jdbc.createConnection();
				ps = con.prepareStatement("DELETE FROM MATRICULA WHERE ID = ?");
				ps.setInt(1, m.getId());
				ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}
		
	}

}
