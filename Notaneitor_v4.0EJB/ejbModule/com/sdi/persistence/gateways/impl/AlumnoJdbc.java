package com.sdi.persistence.gateways.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sdi.model.Alumno;
import com.sdi.model.Asignatura;
import com.sdi.model.Matricula;
import com.sdi.model.Usuario;
import com.sdi.persistence.exception.AlreadyPersistedException;
import com.sdi.persistence.exception.NotPersistedException;
import com.sdi.persistence.exception.PersistenceException;
import com.sdi.persistence.gateways.JdbcHelper;

/**
 * Implementaci��n de la interfaz de fachada al servicio de persistencia
 * para Alumnos. En este caso es Jdbc pero podr��a ser cualquier otra
 * tecnologia de persistencia, por ejemplo, la que veremos m��s adelante JPA
 * (mapeador de objetos a relacional)
 * 
 * @author alb
 * 
 */
public class AlumnoJdbc {
	
	private static String CONFIG_FILE = "/persistence.properties";
	private JdbcHelper jdbc = new JdbcHelper(CONFIG_FILE);


	public List<Alumno> getAlumnos() {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		List<Alumno> alumnos = new ArrayList<Alumno>();

		try {

			
			con = jdbc.createConnection();
			ps = con.prepareStatement("select * from alumno");
			rs = ps.executeQuery();

			while (rs.next()) {
				Alumno alumno = new Alumno();
				alumno.setId(rs.getLong("ID"));
				alumno.setNombre(rs.getString("NOMBRE"));
				alumno.setApellidos(rs.getString("APELLIDOS"));
				alumno.setEmail(rs.getString("EMAIL"));
				alumno.setIduser(rs.getString("IDUSER"));

				alumnos.add(alumno);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception ex) {
				}
			}
			;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}

		return alumnos;
	}

	public void delete(Long id) throws NotPersistedException {
		PreparedStatement ps = null;
		Connection con = null;
		int rows = 0;

		try {
			con = jdbc.createConnection();
			ps = con.prepareStatement("delete from alumno where id = ?");

			ps.setLong(1, id);

			rows = ps.executeUpdate();
			if (rows != 1) {
				throw new NotPersistedException("Alumno " + id + " not found");
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}
	}

	public Alumno findById(Long id) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		Alumno alumno = null;

		try {
			// Obtenemos la conexi��n a la base de datos.
			con = jdbc.createConnection();
			ps = con.prepareStatement("select * from alumno where id = ?");
			ps.setLong(1, id);

			rs = ps.executeQuery();
			if (rs.next()) {
				alumno = new Alumno();

				alumno.setId(rs.getLong("ID"));
				alumno.setNombre(rs.getString("NOMBRE"));
				alumno.setApellidos(rs.getString("APELLIDOS"));
				alumno.setEmail(rs.getString("EMAIL"));
				alumno.setIduser(rs.getString("IDUSER"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception ex) {
				}
			}
			;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}

		return alumno;
	}

	public void save(Alumno a) throws AlreadyPersistedException {
		PreparedStatement ps = null;
		Connection con = null;
		int rows = 0;

		try {
			con = jdbc.createConnection();
			ps = con.prepareStatement("insert into alumno (nombre, apellidos, iduser, email) "
					+ "values (?, ?, ?, ?)");

			ps.setString(1, a.getNombre());
			ps.setString(2, a.getApellidos());
			ps.setString(3, a.getIduser());
			ps.setString(4, a.getEmail());

			rows = ps.executeUpdate();
			if (rows != 1) {
				throw new AlreadyPersistedException("Alumno " + a
						+ " already persisted");
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}
	}

	public void update(Alumno a) throws NotPersistedException {
		PreparedStatement ps = null;
		Connection con = null;
		int rows = 0;

		try {
			con = jdbc.createConnection();
			ps = con.prepareStatement("update alumno "
					+ "set nombre = ?, apellidos = ?, iduser = ?, email = ?"
					+ "where id = ?");

			ps.setString(1, a.getNombre());
			ps.setString(2, a.getApellidos());
			ps.setString(3, a.getIduser());
			ps.setString(4, a.getEmail());
			ps.setLong(5, a.getId());

			rows = ps.executeUpdate();
			if (rows != 1) {
				throw new NotPersistedException("Alumno " + a + " not found");
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}
	}

	public List<Matricula> findMatriculaByID(String id) {
		List<Matricula> matricula = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection con = null;
		
		try{
			con = jdbc.createConnection();
			ps = con.prepareStatement("SELECT a.nombre as nombre, m.nota as nota from matricula m, asignatura a where a.codigo = m.asignatura and m.alumno= ?");
			ps.setString(1, id);
			rs = ps.executeQuery();
			
			matricula = new ArrayList<Matricula>();
			while(rs.next()){
				Matricula m = new Matricula();
				m.setAsignatura(rs.getString("nombre"));
				m.setNota(rs.getString("nota"));
				matricula.add(m);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}
		return matricula;
	}

	public void matricular(Usuario u, Asignatura a) {
		
		PreparedStatement ps = null;
		Connection con = null;
		
		try{
			con = jdbc.createConnection();
			ps = con.prepareStatement("INSERT INTO MATRICULA (ASIGNATURA,ALUMNO) VALUES (?,?)");
			ps.setString(1, a.getCodigo());
			ps.setString(2, u.getLogin());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}
		
	}
	

}
