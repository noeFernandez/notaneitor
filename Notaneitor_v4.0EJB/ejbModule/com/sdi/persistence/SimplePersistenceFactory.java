package com.sdi.persistence;


import com.sdi.persistence.gateways.AlumnoDao;
import com.sdi.persistence.gateways.AsignaturaDao;
import com.sdi.persistence.gateways.UsuarioDao;
import com.sdi.persistence.gateways.persistencefachade.AlumnoJdbcDAO;
import com.sdi.persistence.gateways.persistencefachade.AsignaturaJdbcDAO;
import com.sdi.persistence.gateways.persistencefachade.UsuarioJdbcDAO;

/**
 * Implementaci??????n de la factoria que devuelve implementaci??????n de la capa
 * de persistencia con Jdbc 
 * 
 * @author alb
 *
 */
public class SimplePersistenceFactory implements PersistenceFactory {

	@Override
	public AlumnoDao createAlumnoDao() {
		return new AlumnoJdbcDAO();
	}

	@Override
	public UsuarioDao createUsuarioDao() {
		return new UsuarioJdbcDAO();
	}

	@Override
	public AsignaturaDao createAsignaturaDao() {
		return new AsignaturaJdbcDAO();
	}
	
	

}
