package com.sdi.persistence;

import com.sdi.persistence.gateways.AlumnoDao;
import com.sdi.persistence.gateways.AsignaturaDao;
import com.sdi.persistence.gateways.UsuarioDao;

/**
 * Interfaz de la factoria que suministra implementaciones reales de la fachada
 * de persistencia para cada Entidad del Modelo (en este caso solo hay una:
 * Alumno; pero en futuras versiones habr�� m��s)
 * 
 * @author alb
 * 
 */
public interface PersistenceFactory {

	public AlumnoDao	 createAlumnoDao();

	public UsuarioDao	 createUsuarioDao();

	public AsignaturaDao createAsignaturaDao();
}
