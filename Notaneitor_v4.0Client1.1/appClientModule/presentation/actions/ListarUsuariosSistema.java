package presentation.actions;

import java.util.List;

import javax.ejb.EJB;

import presentation.util.Action;

import com.sdi.ws.EJBUsuariosServiceService;
import com.sdi.ws.Usuario;
import com.sdi.ws.UsuariosService;

public class ListarUsuariosSistema implements Action {
	
	@EJB
	static UsuariosService service;

	@Override
	public void execute() throws Exception {
		service = new EJBUsuariosServiceService().getUsuariosServicePort();

		List<Usuario> usuarios = service.getUsers();

		for (Usuario u : usuarios) {
			System.out.println(u);
		}
		
	}

}
