package presentation.actions;

import javax.ejb.EJB;

import presentation.util.Action;
import presentation.util.Console;

import com.sdi.ws.EJBUsuariosServiceService;
import com.sdi.ws.Usuario;
import com.sdi.ws.UsuariosService;

public class AddUser implements Action {

	private String nombre;
	private String apellidos;
	private String email;
	private String usuario;
	private String pass1;
	private String pass2;
	private String rol = "alumno";
	
	@EJB
	static UsuariosService service;

	@Override
	public void execute() throws Exception {

		nombre = Console.readString("Introduzca el nombre");
		apellidos = Console.readString("Introduzca los apellidos");
		email = Console.readString("Introduzca el email");

		usuario = Console.readString("Introduzca el usuario");

		do {
			pass1 = Console.readString("Introduzca la contraseņa");
			pass2 = Console.readString("Repita la contraseņa");
		} while (!pass1.equals(pass2));

		/*do {
			rol = Console.readString("Intrduzca el rol [alumno|profesor]");
		} while ( !rol.equals("profesor") || !rol.equals("alumno"));*/
		
		
		Usuario us = new Usuario();
		us.setActivada(false);
		us.setName(nombre);
		us.setApellidos(apellidos);
		us.setEmail(email);
		us.setLogin(usuario);
		us.setPassword(pass1);
		us.setRol(rol);
		
		service = new  EJBUsuariosServiceService().getUsuariosServicePort();
		
		service.addNewUser(us);
		
		
		
	}

}
