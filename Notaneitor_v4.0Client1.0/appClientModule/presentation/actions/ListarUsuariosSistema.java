package presentation.actions;

import java.util.List;

import javax.ejb.EJB;

import com.sdi.business.RemoteServiceFactoryImpl;
import com.sdi.business.services.AlumnosService;
import com.sdi.business.services.UsuariosService;
import com.sdi.model.Usuario;

import presentation.util.Action;

public class ListarUsuariosSistema implements Action {
	
	@EJB
	static UsuariosService service;

	@Override
	public void execute() throws Exception {
		service = new RemoteServiceFactoryImpl().createUsuariosService();

		List<Usuario> usuarios = service.getUsers();

		for (Usuario u : usuarios) {
			System.out.println(u);
		}
		
	}

}
