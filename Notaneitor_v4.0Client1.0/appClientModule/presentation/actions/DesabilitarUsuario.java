package presentation.actions;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;

import presentation.util.Action;
import presentation.util.Console;

import com.sdi.business.RemoteServiceFactoryImpl;
import com.sdi.business.services.UsuariosService;
import com.sdi.model.Usuario;

public class DesabilitarUsuario implements Action {
	
	@EJB
	static UsuariosService service;

	@Override
	public void execute() throws Exception {
		
		service = new RemoteServiceFactoryImpl().createUsuariosService();
		List<Usuario> usuarios =  filterByActivated(service.getUsers());
		
		if(usuarios.size() != 0){
			for(Usuario u : usuarios){
				System.out.println(u);
			}
			
			int id;
			do{
				id = Console.readInt("Introduzca un id de usuario");
			}while(!contains(id,usuarios));
			
			Usuario desactivado = getUserByID(id, usuarios);
			
			Console.printf("El usuario %s %s ha sido desactivado", desactivado.getName(),desactivado.getApellidos());

			service.changeActivationState(desactivado);
			
			
		}else{
			System.out.println("No hay usuario activos");
		}
		
		
	}
	
	
	private List<Usuario> filterByActivated(List<Usuario> allusers){
		List<Usuario> usuario = new ArrayList<Usuario>();
		for(Usuario u : allusers){
			if(u.getActivada()){
				usuario.add(u);
			}
		}
		return usuario;
	}

	private boolean contains(int id, List<Usuario> usuarios){
		for(Usuario u : usuarios){
			if(u.getId() == id){
				return true;
			}
		}
		return false;
	}
	
	private Usuario getUserByID(int id, List<Usuario> usuarios){
		for(Usuario u : usuarios){
			if(u.getId() == id ){
				return u;
			}
		}
		throw new IllegalArgumentException();
	}
}
