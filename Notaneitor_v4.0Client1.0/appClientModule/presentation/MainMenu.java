package presentation;

import presentation.util.BaseMenu;

public class MainMenu extends BaseMenu {

    public MainMenu() {
	menuOptions = new Object[][] { { "Menu Principal", null },
		{ "Listar usuarios del sistema", presentation.actions.ListarUsuariosSistema.class },
		{ "Habilitar un usuario", presentation.actions.ActivarUsuarios.class},
		{ "Deshabilitar un usuario", presentation.actions.DesabilitarUsuario.class},
		{ "Nuevo usuario", presentation.actions.AddUser.class},
		
	};
    }

    public static void main(String[] args) {
	new MainMenu().execute();
    }

}
