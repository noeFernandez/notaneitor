package com.sdi.presentation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.sdi.business.services.AlumnosService;
import com.sdi.business.services.AsignaturasService;
import com.sdi.infrastructure.Factories;
import com.sdi.model.Asignatura;
import com.sdi.model.Matricula;
import com.sdi.model.Usuario;
import com.sdi.util.Bundle;

@ManagedBean(name = "matricula")
@SessionScoped
public class BeanMatricula implements Serializable {

	private static final long serialVersionUID = -1479465781361953191L;
	private Usuario alumno;
	private List<Asignatura> asignaturasNoMatriculadas;
	private List<Matricula> asignaturasMatriculadas;

	public Usuario getAlumno() {
		return alumno;
	}

	public void setAlumno(Usuario alumno) {
		this.alumno = alumno;
	}

	public void setAsignaturasNoMatriculadas() {
		AsignaturasService as = Factories.services.createAsignaturasService();
		AlumnosService als = Factories.services.createAlumnoService();

		List<Asignatura> allCourses = as.getAsignaturas();
		List<Matricula> cursosMatriculado = als.obtenerMatricula(alumno
				.getLogin());
		List<Asignatura> asignaturasDisponibles = new ArrayList<Asignatura>();

		for (Asignatura a : allCourses) {
			if (!contiene(a, cursosMatriculado)) {
				asignaturasDisponibles.add(a);
			}
		}

		this.asignaturasNoMatriculadas = asignaturasDisponibles;

	}

	public List<Asignatura> getAsignaturasNoMatriculadas(){
		setAsignaturasNoMatriculadas();
		return this.asignaturasNoMatriculadas;
	}
	
	
	

	public List<Matricula> getAsignaturasMatriculadas() {
		AsignaturasService as = Factories.services.createAsignaturasService();
		this.asignaturasMatriculadas = as.getMatriculasAlumno(alumno);
		return asignaturasMatriculadas;
	}
	
	public void baja(Matricula m){
		AsignaturasService as = Factories.services.createAsignaturasService();
		as.bajaAlumno(m);
	}

	private boolean contiene(Asignatura asignatura, List<Matricula> matriculados) {
		for (Matricula m : matriculados) {
			if (m.getAsignatura().equals(asignatura.getNombre())) {
				return true;
			}
		}
		return false;
	}
	
	public String matricula(Asignatura a){
		try{
		Factories.services.createAlumnoService().matricular(this.alumno,a);
		}catch(Exception e){
			FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage("tabla", new FacesMessage(Bundle.getCurrentBundle().getString("STUDENT_ALREADY_EXISTS")));
		}
		this.setAsignaturasNoMatriculadas();
		return "/restricted/perfilAsignaturasAlumno";
	}
}
