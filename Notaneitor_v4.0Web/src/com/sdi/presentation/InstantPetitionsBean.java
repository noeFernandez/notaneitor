package com.sdi.presentation;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.sdi.model.Usuario;

@ManagedBean(name="instant")
@SessionScoped
public class InstantPetitionsBean implements Serializable{

	private static final long serialVersionUID = 691983652377038914L;
	
	public String homeRequest(){
		if(((Usuario)Session.get("LOGGEDIN_USER")) != null){
			return "/restricted/opciones";
		}else{
			return "index";
		}
	}
	
	public String listadoProfesoresRequest(){
		return "/listadoProfesores";
	}
	
	public String listadoAsignaturasRequest(){
		return "/listadoAsignaturas";
	}

	public String singOutRequest(){
		return "/index";	
	}
	
	public String altaRequest(){
		return "/altaForm";
	}
	
	public String profileEditRequest(){
		return "/restricted/editForm";
	}
	
	public String editRequest(){
		return "/restricted/editForm";
	}
	
	public String asignarAsignaturasRequesr(){
		return "/restricted/perfilAsignaturasProfesor";
	}
	
	public String asignarAsignaturasAlumnoRequest(){
		return "/restricted/perfilAsignaturasAlumno";
	}
	
	public String altaAsignaturaRequest(){
		return "/restricted/altaAsignatura";
	}
	
	public String editarAsignaturaRequest(){
		return "/restricted/altaAsignatura"; 
	}
	
	public String asignarAlumnosAsignaturaRequest(){
		return "/restricted/matriculaAlumno";
	}
	
	public String asignarProfesoresAsignaturaRequest(){
		return "/restricted/asignarProfesor";
	}
	
	public String calificarRequest(){
		return "/restricted/calificar";
	}
	
	public String alumnoBajaAsignaturaRequest(){
		return "/restricted/perfilAsignaturasAlumno";
	}
	
	public String salvarAsignaturaRequest(){
		return "/restricted/opciones";
	}
	
	
}
