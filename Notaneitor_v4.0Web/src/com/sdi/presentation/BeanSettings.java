package com.sdi.presentation;
import java.io.Serializable;
import java.util.Locale;

import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

@ManagedBean(name="settings")
@SessionScoped
public class BeanSettings implements Serializable{
	     private static final long serialVersionUID = 2L;	  	 	  
	     private static final Locale ENGLISH = new Locale("en");
	     private static final Locale SPANISH = new Locale("es");	     
	     private Locale locale = new Locale("es"); 

	   @PreDestroy
	   public void end()
	   {
	     System.out.println("BeanSettings - PreDestroy");
	   }

	     
	     
	      public Locale getLocale() 
	      { 
	    	  //Aqui habria que cambiar algo de código para coger 
              //locale del navegador 
	    	  //la primera vez que se accede a getLocale()
	    	  System.out.println("BeanSettings - idioma: "+locale);
	    	  return(locale); 
	      } 
	      public void setSpanish(ActionEvent event) 
	      { 
	           locale = SPANISH; 	 
	           System.out.println("BeanSettings - idioma - in: "+locale);
	           FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
	      } 
         public void setEnglish(ActionEvent event) 
         { 
           locale = ENGLISH; 
           System.out.println("BeanSettings - idioma - in: "+locale);
           FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
         } 
	      
	}
