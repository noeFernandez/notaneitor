package com.sdi.presentation;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.sdi.business.services.UsuariosService;
import com.sdi.infrastructure.Factories;
import com.sdi.model.Usuario;
import com.sdi.util.Bundle;

@ManagedBean(name = "usuarios")
@SessionScoped
public class BeanUsuarios implements Serializable {

	private static final long serialVersionUID = 7941310132767585725L;

	private Usuario[] users = null;
	private Usuario user;

	private String pass1 = "";
	private String pass2 = "";

	public String getPass1() {
		return pass1;
	}

	public void setPass1(String pass1) {
		this.pass1 = pass1;
	}

	public String getPass2() {
		return pass2;
	}

	public void setPass2(String pass2) {
		this.pass2 = pass2;
	}

	public Usuario getUser() {
		if(this.user == null){
			iniciaUsuario();
		}
		return user;
	}

	public void setUser(Usuario user) {
		System.out.println(user);
		this.user = user;
	}

	@PreDestroy
	public void end() {
		System.out.println("BeanUsuarios - Predestroy");
	}

	public Usuario[] getUsers() {
		return users;
	}

	public void setUsers(Usuario[] users) {
		this.users = users;
	}

	public List<Usuario> getUsuarios() {
		return Factories.services.createUsuariosService().getUsers();
	}


	public void baja(Usuario u) {
		Factories.services.createUsuariosService().removeUsuario(u);
	}

	public String save() {
		UsuariosService us = Factories.services.createUsuariosService();
		
		

		
		try{
		if (user.getId() == null) { // nuevo usuario
			if (pass1.length() != 0 && pass1.equals(pass2)) {
				user.setPassword(pass1);
				us.addNewUser(user);
				return "/index";
			} else {
				FacesContext context = FacesContext.getCurrentInstance();
		        context.addMessage("pass1", new FacesMessage(Bundle.getCurrentBundle().getString("PASSWORDS_NOT_MATCH")));
				return "/altaForm";
			}
		} else {
			
			if (pass1.length() == 0 && pass2.length() == 0 ) {
				us.updateUser(user, user.getPassword());
				return "/restricted/opciones";
			} else { // hay nueva contraseņa
				if (pass1.equals(pass2)) {
					us.updateUser(user, pass1);
					return "/restricted/opciones";
				}else{
					FacesContext context = FacesContext.getCurrentInstance();
			        context.addMessage("pass1", new FacesMessage(Bundle.getCurrentBundle().getString("PASSWORDS_NOT_MATCH")));
					return "/restricted/editForm";
				}
			}
		}
		}catch(Exception e){
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage("nombre", new FacesMessage(Bundle
					.getCurrentBundle().getString("USER_ALREADY_EXISTS")));
	        if(user.getId() != null){
	        	return "restricted/opciones";
	        }
			return "altaForm";
		}
	}

	public void cambiarEstadoActivacion(Usuario u) {

		Factories.services.createUsuariosService().changeActivationState(u);

	}

	public void iniciaUsuario() {
		user = new Usuario();
	}

	public String logout() {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.remove("LOGGEDIN_USER");
		return "/index";
	}
	
	public Usuario getLoggedUser(){
		return Factories.services.createUsuariosService().getUserByID(((Usuario)Session.get("LOGGEDIN_USER")).getLogin());
	}
	
	public String registerRequest(){
		return "exito";
	}
}
