package com.sdi.presentation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.sdi.infrastructure.Factories;
import com.sdi.model.Asignatura;

@ManagedBean(name="busqueda")
@SessionScoped
public class BeanBusqueda implements Serializable{

	private static final long serialVersionUID = -3600650910339937662L;
	
	
	private String nombre;
	
	
	
	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public List<Asignatura> filtrarAsignatura(){
		
		if(nombre.length() == 0){
			return Factories.services.createAsignaturasService().getAsignaturas();
		}
		
		List<Asignatura> filter= new ArrayList<Asignatura>();
		
		for(Asignatura a : Factories.services.createAsignaturasService().getAsignaturas()){
			if(a.getNombre().contains(nombre)){
				filter.add(a);
			}
		}
		
		return filter;
		
	}

}
