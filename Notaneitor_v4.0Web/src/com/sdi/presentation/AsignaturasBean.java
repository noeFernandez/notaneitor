package com.sdi.presentation;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.sdi.business.services.AsignaturasService;
import com.sdi.infrastructure.Factories;
import com.sdi.model.Asignatura;
import com.sdi.model.Matricula;
import com.sdi.model.Usuario;

@ManagedBean(name = "asignaturas")
@SessionScoped
public class AsignaturasBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6818771884269267983L;

	private Asignatura asigantura;
	private Matricula matricula;
	String nota;

	public Matricula getMatricula() {
		return matricula;
	}
	
	

	public String getNota() {
		return nota;
	}



	public void setNota(String nota) {
		this.nota = nota;
	}



	public void setMatricula(Matricula matricula) {
		this.matricula = matricula;
		this.nota = matricula.getNota();
	}

	private List<Usuario> usuariosNoCursanAsignatura = null;

	public List<Usuario> getUsuariosNoCursanAsignatura() {
		return usuariosNoCursanAsignatura;
	}

	public void setUsuariosNoCursanAsignatura(Asignatura a) {
		this.asigantura = a;
		this.usuariosNoCursanAsignatura = Factories.services
				.createAsignaturasService()
				.obtenerAlumnosQueNoCursanAsignatura(a);
	}


	public List<Asignatura> getAsignaturas() {
		return Factories.services.createAsignaturasService().getAsignaturas();
	}

	public Asignatura getAsigantura() {
		return asigantura;
	}

	public void setAsigantura(Asignatura asigantura) {
		this.asigantura = asigantura;
	}

	public void save() {
		AsignaturasService as = Factories.services.createAsignaturasService();

		if (asigantura.getId() == null) { // nueva asignatura
			as.addNewCourse(this.asigantura);
		} else {
			as.updateCourse(this.asigantura);
		}
	}

	public void delete(Asignatura asig) {
		Factories.services.createAsignaturasService().deleteCourse(asig);
	}

	public void iniciaAsignatura() {
		asigantura = new Asignatura();
	}

	public String matricularAlumno(Usuario u){
		Factories.services.createAlumnoService().matricular(u,this.asigantura);
		this.setUsuariosNoCursanAsignatura(this.asigantura);
		return "/restricted/matriculaAlumno";
	}
	
	public List<Matricula> getMatriculaPorAsignatura(){
		return Factories.services.createAsignaturasService().getMatriculasAsignatura(this.asigantura);
	}
	
	public void calificar(Matricula m){
		System.out.println(m.getAlumno());
		System.out.println(asigantura);
		System.out.println(m.getNota());
		AsignaturasService as = Factories.services.createAsignaturasService();
		as.calificarAlumno(m.getAlumno(), asigantura, m.getNota());
		
	}
}
