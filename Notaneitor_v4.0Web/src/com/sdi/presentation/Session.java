package com.sdi.presentation;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;

@Stateless
public class Session {
	
	public static Object get(String key){
		return FacesContext.getCurrentInstance()
		.getExternalContext().getSessionMap().get(key);
	}
	

}
