package com.sdi.presentation;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.sdi.business.services.AsignaturasService;
import com.sdi.infrastructure.Factories;
import com.sdi.model.Asignatura;
import com.sdi.model.Profesor;
import com.sdi.util.Bundle;

@ManagedBean(name = "imparte")
@SessionScoped
public class ImparteBean implements Serializable {

	private static final long serialVersionUID = 3946524575497413555L;

	private Profesor profesor;
	private Asignatura asignatura;

	private List<Profesor> profesoresQueNoImpartenAsignatura;

	public Profesor getProfesor() {
		return profesor;
	}

	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}

	public Asignatura getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(Asignatura asignatura) {
		this.asignatura = asignatura;
	}

	public String asignarAsignatura(Profesor profesor) {
		Factories.services.createAsignaturasService()
				.asignarAsignaturaAProfesor(profesor, asignatura);
		
		return "/restricted/perfilAsignaturasProfesor";
	}

	public String asignarAsignatura(Asignatura asignatura) {
		try{
		Factories.services.createAsignaturasService()
				.asignarAsignaturaAProfesor(profesor, asignatura);
		}catch(Exception e){
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage("asignar", new FacesMessage(Bundle
					.getCurrentBundle().getString("TEACHER_ALREADY_TEACHS")));
		}
		
		return "/restricted/perfilAsignaturasProfesor";
	}

	public void setProfesoresQueNoImpartenAsignatura(Asignatura asignatura) {
		AsignaturasService as = Factories.services.createAsignaturasService();
		this.profesoresQueNoImpartenAsignatura = as
				.obtenerProfesoresQueNoImpartenAsignatura(asignatura);
	}

	public List<Profesor> getProfesoresQueNoImpartenAsignatura() {
		setProfesoresQueNoImpartenAsignatura(this.asignatura);
		return profesoresQueNoImpartenAsignatura;
	}

}
