package com.sdi.presentation.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.sdi.util.Bundle;

@FacesValidator("com.sdi.presentation.validator.ValidadorNota")
public class ValidadorNota implements Validator {

	@Override
	public void validate(FacesContext facesContext, UIComponent component,
			Object obj) throws ValidatorException {
		String nota = obj.toString();
		
		if(!nota.matches("[0-9]'.'([0-9][0-9])?") || !nota.equals("NC") || !nota.equals("NE")){
			System.out.println("ta mal");
			FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage("nota", new FacesMessage(Bundle.getCurrentBundle().getString("INVALID_MARK")));
		}
		
	}

}
