package com.sdi.presentation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.sdi.business.services.AsignaturasService;
import com.sdi.infrastructure.Factories;
import com.sdi.model.Asignatura;
import com.sdi.model.Profesor;
import com.sdi.model.Usuario;

@ManagedBean(name = "profesores")
@SessionScoped
public class ProfesoresBean implements Serializable {

	private static final long serialVersionUID = -9133607118906114386L;
	private Profesor profesor;
	private List<Asignatura> asignaturasQueNoImparte;

	/**
	 * Obtiene todos los profesores de la base de datos
	 * 
	 * @return
	 */
	public List<Profesor> getProfesores() {
		return Factories.services.createUsuariosService().getProfesores();
	}

	public void setUsuario(Usuario profesor) {

		this.profesor = getProfesorByID(profesor);
	}

	public Profesor getProfesor() {
		return this.profesor;
	}

	private void setAsignaturasNoImparteProfesor() {
		AsignaturasService as = Factories.services.createAsignaturasService();
		List<Asignatura> todas = as.getAsignaturas();
		List<Asignatura> asignaturasQueNoImparte = new ArrayList<Asignatura>();
		
		if(profesor.getAsignaturas() == null){
			this.asignaturasQueNoImparte = todas;
			return;
		}

		for (Asignatura asignatura : todas) {
			if (!contiene(asignatura, profesor.getAsignaturas())) {
				asignaturasQueNoImparte.add(asignatura);
			}
		}
		this.asignaturasQueNoImparte = asignaturasQueNoImparte;
	}

	public List<Asignatura> getAsignaturasQueNoImparte() {
		setAsignaturasNoImparteProfesor();
		return asignaturasQueNoImparte;
	}

	private boolean contiene(Asignatura asignatura, List<Asignatura> asignaturas) {
		for (Asignatura m : asignaturas) {
			if (m.getNombre().equals(asignatura.getNombre())) {
				return true;
			}
		}
		return false;
	}

	private Profesor getProfesorByID(Usuario profesor) {
		for (Profesor p : Factories.services.createUsuariosService()
				.getProfesores()) {
			if (p.getLogin().equals(profesor.getLogin())) {
				return p;
			}
		}
		return null;
	}

	
	public List<Asignatura> getMisAsignaturas(){
		Usuario u = (Usuario)Session.get("LOGGEDIN_USER");
		List<Profesor> profesores = Factories.services.createUsuariosService().getProfesores();
		for(Profesor p : profesores){
			if(p.getLogin().equals(u.getLogin())){
				return p.getAsignaturas();
			}
		}
		return null;
	}

}
