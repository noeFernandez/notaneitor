package com.sdi.presentation;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.sdi.business.services.AlumnosService;
import com.sdi.infrastructure.Factories;
import com.sdi.model.Matricula;
import com.sdi.model.Usuario;

@ManagedBean(name = "controller")
@SessionScoped
public class BeanAlumnos implements Serializable {
	private static final long serialVersionUID = 55555L;
	

	public List<Matricula> getMatricula(){
		AlumnosService as = Factories.services.createAlumnoService();
		Usuario user = (Usuario)Session.get("LOGGEDIN_USER");
		return as.obtenerMatricula(user.getLogin());
	}
	
	
	
}
